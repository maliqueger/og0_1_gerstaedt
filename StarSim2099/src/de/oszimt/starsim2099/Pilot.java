package de.oszimt.starsim2099;

/**
 * Write a description of class Pilot here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Pilot {

	// Attribute
	private double posX;
	private double posY;
	private String grad;
	private String name;

	// Methoden
	public Pilot() {;
		this.posX = 0;
		this.posY = 0;
		this.grad = "";
		this.name = "";
	}

	public void setPosX(double posX) {
		this.posX = posX;
	}

	public double getPosX() {
		return posX;
	}
	public void setPosY(double posY) {
		this.posY = posY;

	}
	public double getPosY() {
		return posY;
	}
	public void setGrad(String grad) {
		this.grad = grad;
	}
	public String getGrad() {
		return grad;
	
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String name() {
		return name;
	}
}
