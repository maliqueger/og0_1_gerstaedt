package de.oszimt.starsim2099;

/**
 * Write a description of class Raumschiff here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Raumschiff {

	// Attribute
	private double posX;
	private double posY;
    private int maxLadekapazitaet;
    private int winkel;
    private String antrieb;
    private String typ;
	// Methoden
    public Raumschiff() {;
	this.posX = 0;
	this.posY = 0;
	this.maxLadekapazitaet = 0;
	this.winkel= 0;
	this.antrieb= "";
	this.typ = "";
		}
    
	public void setPosX(double posX) {
		this.posX = posX;
	}

	public double getPosX() {
		return posX;
	}
	public void setPosY(double posY) {
		this.posY = posY;

	}
	public double getPosY() {
		return posY;
	}
	
	public void setMaxLadekapazitaet(int maxLadekapazitaet) {
		this.maxLadekapazitaet = maxLadekapazitaet;
	}
	
	public int getMaxLadekapazitaet() {
		return maxLadekapazitaet;
		
	}
	
	public void setWinkel(int winkel) {
		this.winkel = winkel;
	}
	
	public int getWinkel() {
		
	}
	
	
	// Darstellung
	public static char[][] getDarstellung() {
		char[][] raumschiffShape = { 
				{'\0', '\0','_', '\0', '\0'},
				{'\0', '/', 'X', '\\', '\0'},
				{'\0', '{', 'X', '}', '\0'},
				{'\0', '{', 'X', '}', '\0'},
				{'/', '_', '_','_', '\\'},				
		};
		return raumschiffShape;
	}

}
