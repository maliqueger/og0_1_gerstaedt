package Algorithmen;
import java.util.Scanner;

public class Interpolationssuche {
    public static void main(String[]args) {
        int[] zahlen = {1,4,6,7,9,11,14,17,18,20,23,25};
        Scanner sc = new Scanner(System.in);
        System.out.println("Es wird nach einem Freien Hotelzimmer gesucht");
        System.out.println("Es gibt 25 Zimmer und man soll eine Zahl eingeben um zu gucken ob das Zimmer Frei ist oder nicht");
        double zimmerNr = sc.nextInt();
        double max = zahlen[11];
        double min = zahlen[0];
        boolean zimmerFrei = false;
        double erg = (zimmerNr - min )/(max-min);
        int erg2 = (int) (max*erg);
        if(erg2 > max) {
            System.out.println("Zimmer nicht frei");
            System.exit(0);
        }
        if(erg2 == zimmerNr) {
            System.out.println("Zimmer ist frei");
            zimmerFrei = true;
            }
        if(erg2 < zimmerNr) {
            erg2 = erg2 + 1;
            for(int f = 0;f < zahlen.length;f++) {
                if(erg2 == zahlen[f]) {
                    System.out.println("Zimmer ist frei");
                    zimmerFrei = true;
                    }
                }
        }
        if(erg2 > zimmerNr) {
            erg2 = erg2 -1;
            for(int f = 0;f < zahlen.length;f++) {
                if(erg2 == zahlen[f]) {
                    System.out.println("Zimmer ist frei");
                    zimmerFrei = true;
                    }
                }
        }
        if(zimmerFrei == false) {
            System.out.println("Zimmer nicht frei");
        }
    }
}