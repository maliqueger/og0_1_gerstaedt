package Algorithmen;
import java.util.Arrays;

public class BubbleSortTest {
	public static void main (String[]args){
		BubbleSort bs = new BubbleSort();
		
		long[] zahlenliste = {5,4,8,3,9,7,6,2,1};
		
		// Vor Sortierung
		System.out.println(Arrays.toString(zahlenliste));
		
		// Sortierung
		bs.sortiere(zahlenliste);
		
		// Nach Sortierung
		System.out.println(Arrays.toString(zahlenliste));
		System.out.println(bs.getVertauschungen());
	}
}